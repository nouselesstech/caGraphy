# caGraphy
TUI application for understanding CA environment.

## Use 
1. Compilfe from source with `cargo build`
2. Navigate to the build location `cd target\debug`
3. Run the script `caGraphy.exe --tenant-id '<tenantId>' --client-id '<clientID>'`