mod msft_apis;
use clap::Parser;
use crate::msft_apis::graph_api::*;
use crate::msft_apis::wallet::*;
use crate::msft_apis::tenant_context::*;
use std::process::Command;


#[derive(Parser,Debug)]
#[command(version, about, long_about = None)]
struct Args {
    #[arg(short, long)]
    tenant_id: String,
    
    #[arg(short, long)]
    client_id: String
}

fn main() {
    // Clear the screen because I suck.
    Command::new("clear").status().unwrap();

    // Tell the people what they got theyselves into.
    println!("CA Graphy. A tool to graph out CA policies.");
    println!("Let's start with getting some credentials.");

    
    // Get the tenant context 
    let args = Args::parse();
    let tenant_context = TenantContext {
        tenant_id: args.tenant_id,
        client_id: args.client_id
    };

    // Setup the token wallet
    let mut wallet = Wallet {
        graph_token: "".to_string()
    };
    wallet.get_graph_token(&tenant_context);


    println!("{}", wallet.graph_token);
}
