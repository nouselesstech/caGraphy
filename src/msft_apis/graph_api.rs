use crate::msft_apis::tenant_context::*;
use reqwest::blocking::*;

pub enum AuthType {
    // AuthorizationCode,
    // ClientCredentials,
    DeviceCode,
    // IntegratedAuth
}

pub fn graph_api_authn(auth_type: AuthType, tenant_context: &TenantContext) -> String {
    match auth_type{
        // AuthType::AuthorizationCode=>return "Authorization Code it is".to_string(),
        // AuthType::ClientCredentials=> {
        //     return "Client Crednetials it is".to_string()
        // },
        AuthType::DeviceCode=> {
            return device_code_flow(tenant_context);
        },
        // AuthType::IntegratedAuth=>return "Integrated Auth it is".to_string()        
    }   
}

pub fn device_code_flow(tenant_context: &TenantContext ) -> String {
    println!("Starting Device Code Flow."); 

    let uri = "https://example.com";
    let body = start_device_code_flow(tenant_context).unwrap(); 

    println!("{}", body);

    return "bleh".to_string();
}

pub fn start_device_code_flow(tenant_context: &TenantContext) -> Result<String, Box<dyn std::error::Error>> {
    let uri = "https://example.com";
    let body = reqwest::blocking::get(uri)?.text()?;
    return Ok(body)
}