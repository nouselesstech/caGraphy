use crate::msft_apis::graph_api::*;
use crate::AuthType::DeviceCode;
use crate::msft_apis::tenant_context::*;

pub struct Wallet {
    pub graph_token: String
}

impl Wallet {
    pub fn get_graph_token(&mut self, tenant_context: &TenantContext) {
        self.graph_token = graph_api_authn(DeviceCode, tenant_context)
    }
}